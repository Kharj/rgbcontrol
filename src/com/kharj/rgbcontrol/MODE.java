package com.kharj.rgbcontrol;

public final class MODE {
	public static final int slow = 0;
	public static final int one = 1;
	public static final int strobo = 2;
	public static final int sound_1 = 3;
	public static final int sound_2 = 4;
	public static final int strobo_ext = 5;
}
