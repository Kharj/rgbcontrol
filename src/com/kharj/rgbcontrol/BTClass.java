package com.kharj.rgbcontrol;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.bluetooth.*;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

public class BTClass {
	private static final String TAG = "bluetooth2";
	   
	  Handler h;
	   
	  private static final int REQUEST_ENABLE_BT = 1;
	  private static final int TIMEOUT = 1000;
	  final int RECIEVE_MESSAGE = 1;		// ������ ��� Handler
	  private BluetoothAdapter btAdapter = null;
	  private BluetoothSocket btSocket = null;
	  private MainActivity parentActivity = null;
	  
	  private ConnectedThread mConnectedThread;
	 //Buffer
	 
	  
	  // SPP UUID ������� 
	  private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
	 
	  // MAC-����� Bluetooth ������
	  private static String address = "20:13:10:17:15:34";
	  //private static String address = "00:1F:81:00:08:30"; //pc
	  
	  public BTClass(MainActivity act){
		  parentActivity = act;
		  h = new Handler() {
		    	public void handleMessage(android.os.Message msg) {
		    		switch (msg.what) {
		            case RECIEVE_MESSAGE:													// ���� ������� ��������� � Handler
		            	byte[] readBuf = (byte[]) msg.obj;
		            	int byteCount = msg.arg1;
		            	if(byteCount==8){
		            		parentActivity.processMessage(readBuf);
		            	}
		            	break;
		    		}
		        };
			};
		     
		    btAdapter = BluetoothAdapter.getDefaultAdapter();		// �������� ��������� Bluetooth �������
		    checkAdapterState();
	  }
	  public void changeMode(int m){
		  if(!isConnected()) return;
		  int [] msg = new int [8];
		  msg[0]=11;
		  msg[1]=m;		  
		//mConnectedThread.write(msg);
		  write(msg);
	  }
	  public void setOnOff(boolean a){
		  if(!isConnected()) return;
		  int [] msg = new int [8];
		  msg[0]=3;
		  msg[1]=100;
		  msg[2]=100;
		  msg[3]=100;
		  if(a) 
			  msg[4]=255;
		  else
			  msg[4]=0;
		//mConnectedThread.write(msg);
		  write(msg);
	  }
	  public void setOneColor(int r, int g, int b){
		  if(!isConnected()) return;
		  int [] msg = new int [8];
		  msg[0]=4;
		  msg[1]=r;
		  msg[2]=g;
		  msg[3]=b;
		  msg[4]=255;
		//mConnectedThread.write(msg);
		  write(msg);
	  }
	  public void setStrobo(int m, int l, int d){
		  if(!isConnected()) return;
		  int [] msg = new int [8];
		  msg[0]=8;
		  msg[1]=m;
		  msg[2]=l;
		  msg[3]=d;
		//mConnectedThread.write(msg);
		  write(msg);
	  }
	  public void resetSound(){
		  if(!isConnected()) return;
		  int [] msg = new int [8];
		  msg[0]=12;
		  msg[1]=200;
		  msg[2]=42;
		  msg[3]=89;
		  msg[4]=61;
		  msg[5]=2;
		//mConnectedThread.write(msg);
		  write(msg);
	  }
	  public void request(int a){
		  if(!isConnected()) return;
		  if(a!=100 && a!= 10 && a!= 2 && a!= 5 && a!= 7 && a!= 9 && a!= 13 && a!=14) return;
		  int [] msg = new int [8];
		  msg[0]=a;	  
		 write(msg);
	  }

	  private void write(int [] msg){
		  if(!isConnected()) return;
		  Log.d(TAG,"write");
		  try {
				Thread.sleep(250);
			} catch (InterruptedException e) {	  }
		  //if(mConnectedThread.busy>0) return;
			 mConnectedThread.write(msg);
			 try {
				Thread.sleep(250);
			} catch (InterruptedException e) {	  }
	  }
	  public void connect(){
		    Log.d(TAG, "...onResume - ������� ����������...");
		    
		    // Set up a pointer to the remote node using it's address.
		   
		    // Two things are needed to make a connection:
		    //   A MAC address, which we got above.
		    //   A Service ID or UUID.  In this case we are using the
		    //     UUID for SPP.
		    try {

			  BluetoothDevice device = btAdapter.getRemoteDevice(address);
		      btSocket = device.createRfcommSocketToServiceRecord(MY_UUID);
		    } catch (IOException e) {
		    	Toast.makeText(parentActivity, "In onResume() and socket create failed: " + e.getMessage() + ".", Toast.LENGTH_LONG).show();
		    }catch (IllegalArgumentException e){
		    	Toast.makeText(parentActivity, "Wrong BT address", Toast.LENGTH_LONG).show();
		    		    }
		   
		    // Discovery is resource intensive.  Make sure it isn't going on
		    // when you attempt to connect and pass your message.
		    btAdapter.cancelDiscovery();
		   
		    // Establish the connection.  This will block until it connects.
		    Log.d(TAG, "...�����������...");
		    try {
		      btSocket.connect();
		      Log.d(TAG, "...���������� ����������� � ������ � �������� ������...");
		    } catch (IOException e) {
		      try {
		        btSocket.close();
		      } catch (IOException e2) {
		    	  Toast.makeText(parentActivity, "In onResume() and unable to close socket during connection failure" + e2.getMessage() + ".", Toast.LENGTH_LONG).show();
		      }
		    }
		     
		    // Create a data stream so we can talk to server.
		    Log.d(TAG, "...�������� Socket...");
		   try{
			   mConnectedThread = new ConnectedThread(btSocket);
			   mConnectedThread.start();
		   }catch(Exception ex){
			   Toast.makeText(parentActivity, "Thread start error" + ex.getMessage() + ".", Toast.LENGTH_LONG).show();      
		   }
	  }
	  public void disconnect(){
		  Log.d(TAG, "...In onPause()...");
		    try{
			  mConnectedThread.cancel();
			  mConnectedThread.interrupt();
		      btSocket.close();
			 // mConnectedThread.stop();
		    } catch (IOException e2) {
		    	 Toast.makeText(parentActivity, "In onPause() and failed to close socket." + e2.getMessage() + ".", Toast.LENGTH_LONG).show();
		    } 
	  }
	  public boolean isConnected(){
		  try{
			 return btSocket.isConnected();
		  }catch(Exception ex){
			  return false;
		  }  
	  }
	  public void checkAdapterState(){
		  // Check for Bluetooth support and then check to make sure it is turned on
		    // Emulator doesn't support Bluetooth and will return null
		    if(btAdapter==null) { 
		      Toast.makeText(parentActivity, "Bluetooth not supported", Toast.LENGTH_LONG).show();
		      parentActivity.finish();
		    } else {
		      if (btAdapter.isEnabled()) {
		        Log.d(TAG, "...Bluetooth on");
		      } else {
		        //Prompt user to turn on Bluetooth
		        Intent enableBtIntent = new Intent(btAdapter.ACTION_REQUEST_ENABLE);
		        try{
		        	parentActivity.startActivityForResult(enableBtIntent, 0);
		        }catch(Exception e){
		        	Toast.makeText(parentActivity, e.toString(), Toast.LENGTH_LONG).show();
		 		   
		        }
		      }
		    }
	  }
	  
	  //thread
	  private class ConnectedThread extends Thread {
		  public int busy = 0;
		    private final BluetoothSocket mmSocket;
		    private final InputStream mmInStream;
		    private final OutputStream mmOutStream;
		    private byte [] prevBuffer = new byte[0]; //���������� ����� ��� �������
	        byte[] threadBuffer = new byte[256];  // buffer store for the stream
		 
		    public ConnectedThread(BluetoothSocket socket) {
		        mmSocket = socket;
		        InputStream tmpIn = null;
		        OutputStream tmpOut = null;
		 
		        // Get the input and output streams, using temp objects because
		        // member streams are final
		        try {
		            tmpIn = socket.getInputStream();
		            tmpOut = socket.getOutputStream();
		        } catch (IOException e) {
		        	
		        }
		 
		        mmInStream = tmpIn;
		        mmOutStream = tmpOut;
		    }
		 
		    public void run() {
		        int bytes=0; // bytes returned from read()
		        int available=0;
		        // Keep listening to the InputStream until an exception occurs
		        while (!Thread.currentThread().isInterrupted()) {
		        	try {
		                // Read from the InputStream
		        		available=mmInStream.available();
		        		if(available!=0){
		        			bytes = mmInStream.read(threadBuffer);		// �������� ���-�� ���� � ���� �������� � �������� ������ "buffer"
		        			if(bytes == 8){
		        				prevBuffer = new byte[0];
		        				byte [] resBuffer = new byte[8];
		        				for(int i=0;i<8;i++) resBuffer[i] = threadBuffer[i];
		        				h.obtainMessage(RECIEVE_MESSAGE, resBuffer.length, -1, resBuffer).sendToTarget();		// ���������� � ������� ��������� Handler
		        				busy--;
		        			}else if(prevBuffer.length + bytes == 8){ //���� �������
		        				byte [] resBuffer = new byte[8];
		        				for(int i=0;i<prevBuffer.length;i++) resBuffer[i] = prevBuffer[i];
		        				for(int i=prevBuffer.length;i<8;i++) resBuffer[i] = threadBuffer[i-prevBuffer.length];
		        				h.obtainMessage(RECIEVE_MESSAGE, resBuffer.length, -1, resBuffer).sendToTarget();		// ���������� � ������� ��������� Handler
		        				prevBuffer = new byte[0];		        				
		        				busy--;
		        			}else if(bytes<8){
		        				prevBuffer = new byte [bytes];
		        				for(int i=0;i<bytes;i++) prevBuffer[i] = threadBuffer[i];
		        			
		        			}
		        		}
		        	} catch (IOException e) {
                    	//this.notify();
                    	Log.d(TAG, "IOException");
                    	//Toast.makeText(parentActivity, "IOException", Toast.LENGTH_LONG).show();
		                break;
		            }
		        }
		        
		    }
		 
		    /* Call this from the main activity to send data to the remote device */
		    public void write(int [] message) {
		    	//Log.d(TAG, "...������ ��� ��������: " + message + "...");
		    	//byte[] msgBuffer = message.getBytes();
		    	if(message.length!=8){
		    		return;
		    	}
		    	byte [] send_msg = new byte[8];
		    	for(int i=0;i<8;i++){
		    		if(message[i]>255)
		    			message[i]=255;
		    		if(message[i]<0)
		    			message[i] = 0;
		    			send_msg[i] = (byte) (0xFF & message[i]);
		    	}
		    	try {
		            mmOutStream.write(send_msg);
		            busy++;
		        } catch (IOException e) {
		            Log.d(TAG, "....������ �������� ������: " + e.getMessage() + "...");     
		        }
		    }
		 
		    /* Call this from the main activity to shutdown the connection */
		    public void cancel() {
		        try {
		            mmSocket.close();
		        } catch (IOException e) { }
		    }
		}
}
