package com.kharj.rgbcontrol;
//modes



import android.os.Bundle;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.bluetooth.*;
import android.view.*;
import android.widget.*;
import android.widget.SeekBar.OnSeekBarChangeListener;
//import android.content.DialogInterface;
//import android.content.DialogInterface.OnClickListener;
import android.view.View.OnClickListener;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TabHost.OnTabChangeListener;
public class MainActivity extends Activity
implements OnCheckedChangeListener, OnClickListener, OnTabChangeListener, OnSeekBarChangeListener {
	private static final String TAG = "bluetooth2";

	  private static final int REQUEST_ENABLE_BT = 1;
	private BTClass bt = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        //tabs control
        TabHost tabs = (TabHost) findViewById(android.R.id.tabhost);

		tabs.setup();

		TabHost.TabSpec spec;
		
		spec = tabs.newTabSpec("tab_slow");
		spec.setContent(R.id.tab_slow);
		spec.setIndicator("Slow");
		tabs.addTab(spec);

		spec = tabs.newTabSpec("tab_one");
		spec.setContent(R.id.tab_one);
		spec.setIndicator("One");
		tabs.addTab(spec);

		spec = tabs.newTabSpec("tab_strobo");
		spec.setContent(R.id.tab_strobo);
		spec.setIndicator("Strobo");
		tabs.addTab(spec);
		
		spec = tabs.newTabSpec("tab_sound");
		spec.setContent(R.id.tab_sound);
		spec.setIndicator("Sound");
		tabs.addTab(spec);
		
		spec = tabs.newTabSpec("tab_soundAnalog");
		spec.setContent(R.id.tab_soundAnalog);
		spec.setIndicator("Analog");
		tabs.addTab(spec);

		tabs.setCurrentTab(0);
      
		//Listeners
		tabs.setOnTabChangedListener(this);
		ToggleButton buttonOnOff = (ToggleButton)findViewById(R.id.buttonOnOff);
		buttonOnOff.setOnCheckedChangeListener(this);

		ToggleButton buttonAdditionalOnOff = (ToggleButton)findViewById(R.id.buttonAdditionalOnOff);
		buttonAdditionalOnOff.setOnCheckedChangeListener(this);
		
		 bt = new BTClass(this);
		 //seekbars rgb
		 SeekBar seekBarR = (SeekBar)findViewById(R.id.seekBarR);
		 SeekBar seekBarG = (SeekBar)findViewById(R.id.seekBarG);
		 SeekBar seekBarB = (SeekBar)findViewById(R.id.seekBarB);
		 seekBarR.setBackgroundColor(Color.RED);
		 seekBarG.setBackgroundColor(Color.GREEN);
		 seekBarB.setBackgroundColor(Color.BLUE);
		 seekBarR.setOnSeekBarChangeListener(this);
		 seekBarG.setOnSeekBarChangeListener(this);
		 seekBarB.setOnSeekBarChangeListener(this);
		 Button buttonSendOne = (Button)findViewById(R.id.buttonSendOne);
		 buttonSendOne.setOnClickListener(this);
		 //strobo

		 SeekBar seekBarStroboL = (SeekBar)findViewById(R.id.seekBarLight);
		 SeekBar seekBarStroboD = (SeekBar)findViewById(R.id.seekBarDelay);
		 seekBarStroboL.setOnSeekBarChangeListener(this);
		 seekBarStroboD.setOnSeekBarChangeListener(this);
		 Button buttonStroboSend = (Button)findViewById(R.id.buttonSendStrobo);
		 buttonStroboSend.setOnClickListener(this);
		 ToggleButton buttonStroboMode = (ToggleButton)findViewById(R.id.buttonStroboMode);
		 buttonStroboMode.setOnCheckedChangeListener(this);
		 //sound
		 
		 Button buttonResetSound = (Button)findViewById(R.id.buttonResetSound);
		 buttonResetSound.setOnClickListener(this);
		 
		 
    }
    
    private void showTab(int a){
        //tabs control
        TabHost tabs = (TabHost) findViewById(android.R.id.tabhost);
        if(a<4){
        	tabs.setCurrentTab(a);
        	updateTab(a);
        	
        }
    }
    private void updateTab(int a){
        //tabs load from avr
    	if(a==1){//one
    		bt.request(5);
    	}
    	if(a==2){//strobo
    		bt.request(9);
    	}
    }
	public void processMessage(byte [] bMsg){
		if(bMsg.length!=8) return;
		int [] msg = new int[8];
		for(int i=0;i<8;i++) if(bMsg[i]==-1) msg[i]=255; else msg[i]=bMsg[i];
		Log.d(TAG, "Process msg "+msg[0]+","+msg[1]+","+msg[2]+","+msg[3]+","+msg[4]+","+msg[5]+","+msg[6]+","+msg[7]);
		  switch(msg[0]){
		  case 10:// mode
			  int mode = (int)msg[1];
			  switch(mode){
			  case MODE.slow: showTab(0); break;
			  case MODE.one: 
				  bt.request(5);
				  showTab(1);
				  break;
			  case MODE.strobo:
			  case MODE.strobo_ext:
				  showTab(2); break;
			  case MODE.sound_1:
				  showTab(3); break;
			  case MODE.sound_2:
				  showTab(4); break;
			  }
			  break;
		  case 2://calibration
				  ToggleButton buttonOnOff = (ToggleButton)findViewById(R.id.buttonOnOff);
					buttonOnOff.setChecked((msg[4]!=0));
			  break;
		  case 5://one color
			  SeekBar seekBarR = (SeekBar)findViewById(R.id.seekBarR);
				 SeekBar seekBarG = (SeekBar)findViewById(R.id.seekBarG);
				 SeekBar seekBarB = (SeekBar)findViewById(R.id.seekBarB);
				 TextView textExample = (TextView)findViewById(R.id.textViewExample);
				 int pR = msg[1];
				 int pG = msg[2];
				 int pB = msg[3];
				 seekBarR.setProgress(pR);
				 seekBarG.setProgress(pG);
				 seekBarB.setProgress(pB);
				 textExample.setTextColor(0xff000000 + pR * 0x10000 + pG * 0x100 + pB);
				break;
		  case 8: //strobo
			  int stroboMode = msg[1];
			  int stroboTime = msg[2];
			  int stroboDelay = msg[3];
			  SeekBar seekBarStroboL = (SeekBar)findViewById(R.id.seekBarLight);
			 SeekBar seekBarStroboD = (SeekBar)findViewById(R.id.seekBarDelay);
			 seekBarStroboL.setProgress(stroboTime-1);
			 seekBarStroboD.setProgress(stroboDelay-1);

			 ToggleButton buttonStroboMode = (ToggleButton)findViewById(R.id.buttonStroboMode);
			 buttonStroboMode.setChecked((stroboMode>0));
			 
			 
		  break;
		  }
	  }
    @Override
    public void onResume() {
      super.onResume();
      bt.connect();
      if(bt.isConnected()){
    	  bt.request(10);
    	  bt.request(2);
      }
      
    }
    
    @Override
    public void onPause() {
      super.onPause();
      bt.disconnect();
      
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
  /*  @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    
      if (data == null) {return;}
      String action = data.getAction();
      if(action == BluetoothAdapter.ACTION_REQUEST_ENABLE){
    	   Toast.makeText(getBaseContext(), "You did not allowed Bluetooth", Toast.LENGTH_LONG).show();
		    this.finish();
      }
      //super.onActivityResult(requestCode, resultCode, data);
    }
    */

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		// TODO Auto-generated method stub
		if(buttonView.getId() == R.id.buttonOnOff){
			bt.setOnOff(isChecked);
		}else if(buttonView.getId()== R.id.buttonAdditionalOnOff){//additional
			Log.d(TAG, "14");
			bt.request(14);
		
		}
        
	}

	@Override
	public void onTabChanged(String arg0) {
		// TODO Auto-generated method stub
		if(arg0=="tab_slow"){//Slow mode
			bt.changeMode(MODE.slow);
		}else if(arg0=="tab_one"){//Slow mode
			bt.changeMode(MODE.one);
			updateTab(1);
			//bt.request(5);
			//read colors
		}else if(arg0=="tab_strobo"){//Slow mode
			bt.changeMode(MODE.strobo);
			updateTab(2);
			//bt.request(5); 
		}else if(arg0=="tab_sound"){//Slow mode
			bt.changeMode(MODE.sound_1);
		}else if(arg0=="tab_soundAnalog"){//Slow mode
			bt.changeMode(MODE.sound_2);
		}
	    
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		if(arg0.getId()==R.id.buttonSendOne){//send one color

			SeekBar seekBarR = (SeekBar)findViewById(R.id.seekBarR);
			 SeekBar seekBarG = (SeekBar)findViewById(R.id.seekBarG);
			 SeekBar seekBarB = (SeekBar)findViewById(R.id.seekBarB);
			 int pR = seekBarR.getProgress();
			 int pG = seekBarG.getProgress();
			 int pB = seekBarB.getProgress();
			 bt.setOneColor(pR,  pG,  pB);
		
		}else if(arg0.getId()==R.id.buttonSendStrobo){
			SeekBar seekBarL = (SeekBar)findViewById(R.id.seekBarLight);
			 SeekBar seekBarD = (SeekBar)findViewById(R.id.seekBarDelay);
			 ToggleButton buttonStroboMode = (ToggleButton)findViewById(R.id.buttonStroboMode);
			 int pM = (buttonStroboMode.isChecked())?1:0;//moe
			 int pL = seekBarL.getProgress() + 1;//time
			 int pD = seekBarD.getProgress() + 1;//delay
			 
			 bt.setStrobo(pM,  pL,  pD);
		
		}else if(arg0.getId() == R.id.buttonResetSound){
			
			bt.resetSound();
		}
		
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		// TODO Auto-generated method stub
		SeekBar seekBarR = (SeekBar)findViewById(R.id.seekBarR);
		 SeekBar seekBarG = (SeekBar)findViewById(R.id.seekBarG);
		 SeekBar seekBarB = (SeekBar)findViewById(R.id.seekBarB);
		 TextView textExample = (TextView)findViewById(R.id.textViewExample);
		 int pR = seekBarR.getProgress();
		 int pG = seekBarG.getProgress();
		 int pB = seekBarB.getProgress();
		 textExample.setTextColor(0xff000000 + pR * 0x10000 + pG * 0x100 + pB);
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub\
		//seekbars rgb
		 
		
	};
}
